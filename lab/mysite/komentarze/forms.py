from django import forms


class CommentForm(forms.Form):
    username = forms.CharField()
    content = forms.CharField(widget=forms.Textarea)