# -*- encoding: utf-8 -*-
from cms.menu_bases import CMSAttachMenu
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
import datetime
import views
from models import Entry

class BlogMenu(CMSAttachMenu):
    name = _("Blog Menu")

    def get_nodes(self, request):
        nodes = []
        url = reverse('users_list')
        userslist = NavigationNode(
            u'Lista użytkowników', url, 1)
        nodes.append(userslist)
        url = reverse('months_list')
        monthslist = NavigationNode(
             u'Miesiące', url, 2)
        nodes.append(monthslist)

        if request.user.is_authenticated():
            url = reverse('blog_logout')
            login = NavigationNode(u'Wyloguj się', url, 3)
            nodes.append(login)
            url = reverse('blog_add')
            add = NavigationNode(u'Dodaj wpis', url, 4)
            nodes.append(add)
        else:
            url = reverse('blog_login')
            login = NavigationNode(u'Zaloguj się',url, 3)
            nodes.append(login)
            url = reverse('blog_signup')
            signup = NavigationNode(u'Rejestracja', url, 4)
            nodes.append(signup)


        users = User.objects.all()
        for user in users:
            url = reverse('entries_view', args=[user.username])
            nodes.append(
                NavigationNode(
                    user.username, url, user.pk+10, 1)
            )

        months = Entry.objects.raw("SELECT id, strftime('%%m', date) as month, date FROM blog_entry GROUP BY strftime('%%m', date)")
        for item in months:

            url = reverse('entries_month', args=[item.month])
            nodes.append(NavigationNode(datetime.datetime.strftime(item.date, '%B'),url, item.id+10,2))
        return nodes

menu_pool.register_menu(BlogMenu)