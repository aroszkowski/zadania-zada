from django.conf.urls import patterns, include, url
from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView, DetailView
from django.http import HttpResponse
from models import *
from views import *
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
                       url(r'^$', ListView.as_view(
                           queryset=Entry.objects.order_by('-date'),
                           context_object_name='entries',
                           template_name="blog/list.html"
                       ), name='home'),
                       url(r'^edit/(?P<pk>\d+)/$', blog_edit, name='blog_edit'),
                       url(r'^add/',contact, name='blog_add'),
                       url(r'^login/',blog_login, name='blog_login'),
                       url(r'^logout/',blog_logout, name='blog_logout'),
                       url(r'^signup/',blog_signup, name='blog_signup'),
                       url(r'^entries/(?P<username>\w+)/$', entires_view, name='entries_view' ),
                       url(r'^userslist$', ListView.as_view(
                           template_name='blog/userslist.html',
                           queryset=User.objects.all(),
                           context_object_name='Users',
                           ), name='users_list'),
                       url(r'^entries_m/(?P<month>.+)/$', entries_username_month_view, name="entries_month"),
                       url(r'^monthlist$', ListView.as_view(
                           template_name='blog/monthslist.html',
                           queryset=Entry.objects.raw("SELECT id, strftime('%%m', date) as month, date FROM blog_entry GROUP BY strftime('%%m', date)"),
                           context_object_name='Months',
                           ), name='months_list'),
)


