# -*- encoding: utf-8 -*-
from django.test import TestCase
from models import *

class MicroblogTest(TestCase):

    def setUp(self):
        self.usrn = 'test'
        self.usrp = 'test'
        self.user = User.objects.create(username=self.usrn,password=self.usrp)
        self.user.full_clean()
        self.user.save()
        self.usrn1 = 'test1'
        self.usrp1 = 'test1'
        self.user1 = User.objects.create(username=self.usrn1,password=self.usrp1)
        self.user1.full_clean()
        self.user1.save()
        self.ex_title1 = "Test title1"
        self.ex_text1 = "test text1"
        self.ex_author1 = self.user1
        self.message1 = Entry.objects.create(
            title=self.ex_title1,
            text=self.ex_text1,
            author=self.ex_author1)
        self.ex_title = "Test title"
        self.ex_text = "test text"
        self.ex_author = self.user
        self.message = Entry.objects.create(
            title=self.ex_title,
            text=self.ex_text,
            author=self.ex_author  )

    def test_message_display(self):
        self.assertEquals(unicode(self.message), self.ex_title)
        self.assertEquals(unicode(self.message.text), self.ex_text)
        self.assertEquals(unicode(self.message.title), self.ex_title)
        self.assertEquals(unicode(self.message.author.username), self.ex_author.username)

    def test_Entires(self):
        urlresp = self.client.get('/blog', follow=True)
        self.assertTrue(self.ex_title and self.ex_text and self.ex_text1 and self.ex_title1 in unicode(urlresp))
        print 'Wyswietla wpisy'
    def test_Entries_user(self):
        urlresp = self.client.get('/blog/entries/%s'%self.user.username, follow=True)
        self.assertTrue(self.user.username and self.message.title and self.message.text in unicode(urlresp))
        self.assertFalse(self.user1.username and self.message1.title and self.message1.text in unicode(urlresp))
        print 'Wyswietla wpisy wybranego usera'
    def test_add_Entry(self):
        urlrepnoauth = self.client.get('/blog/add', follow=True)
        self.assertTrue('You have no permission to add entry' in unicode(urlrepnoauth))
        print u'Trzeba się zalogować zeby dodawać wpisy'
        urlrepauth = self.client.post('/blog/signup/',{'username': 'tester',
                                                      'password': 'tester',
                                                      }, follow=True)
        urlrepauthlogin = self.client.post('/blog/login/',{'username': 'tester',
                                                       'password': 'tester',
                                                       }, follow=True)
        urlrepauthadd = self.client.post('/blog/add/',{'title': 'Dodane po zalogowaniu',
                                                    'text': 'Text po zalogowaniu',
                                                  }, follow=True)


        self.assertTrue('The entry has been added successfully' in unicode(urlrepauthadd))
        print u'Dodany wpis po zalogowaniu'
    def test_auth(self):
        urlrepauth = self.client.post('/blog/signup/',{'username': 'tester',
                                                       'password': 'tester',
                                                       }, follow=True)
        urlrepauthlogin = self.client.post('/blog/login/',{'username': 'tester',
                                                       'password': 'tester',
                                                       }, follow=True)
        self.assertTrue('You have been successfully logged in' and 'Logout(tester)' in unicode(urlrepauthlogin))
        print u'Autentykacja uzytkownika'

    def test_messages(self):
        urlrepauthlogin = self.client.post('/blog/login/',{'username': 'tester',
                                                           'password': 'tester',
                                                           }, follow=True)
        self.assertTrue('Invalid username or password' in unicode(urlrepauthlogin))
        print u'Komunikaty np. gdy zle haslo badz login'