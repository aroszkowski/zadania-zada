from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _
from models import LubietoButton

class Lubieto(CMSPluginBase):
    model = LubietoButton
    name = _("Lubieto Plugin") # Name of the plugin
    render_template = "lubieto.html" # template to render the plugin with

    def render(self, context, instance, placeholder):
        context.update({'instance':instance})
        return context

plugin_pool.register_plugin(Lubieto)