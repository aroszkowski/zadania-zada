from django.db import models
from cms.models import CMSPlugin
from django.db import models
from django.utils.translation import ugettext_lazy as _


class LubietoButton(CMSPlugin):
    pageurl = models.URLField(_("URL do polubienia"))

    width = models.PositiveSmallIntegerField(_("Width"), default=None, null=True,
                                             blank=True, help_text=_("Zostaw puste do automatycznego dopasowania"))


    show_faces = models.BooleanField(_("Pokazuj twarze"),
                                     default=True)



    def __unicode__(self):
        return "Like (%s)" % (self.pageurl)
