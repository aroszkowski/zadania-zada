# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from forms import ContactForm
from django.core.mail import send_mail
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.contrib import messages


def kontakt(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['temat']
            message = form.cleaned_data['tresc']
            sender = form.cleaned_data['sender']
            recipients = ['adas.roszkowski@gmail.com']

            send_mail(subject, message, sender, recipients, fail_silently=True)
            messages.add_message(request,messages.INFO, u'Mail wysłany')
            redirect('kontakt')
    else:
        form = ContactForm()
    return render(request, 'contact.html', locals())
