from django import forms

class ContactForm(forms.Form):
    temat = forms.CharField(max_length=100)
    tresc = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()