from django.db import models
from django.contrib.auth.models import User


class Tags(models.Model):
    name = models.CharField(max_length=40)
    def __unicode__(self):
        return self.name
class Entry(models.Model):
    title = models.CharField(max_length=40)
    text = models.CharField(max_length=400)
    date = models.DateTimeField(auto_now_add=True)
    lastmodified = models.DateTimeField(null=True, blank=True)
    lasteditby = models.CharField(max_length=40, null=True, blank=True)
    author = models.ForeignKey(User)
    tags = models.ManyToManyField(Tags, blank= True, null=True)
    def __unicode__(self):
        return self.title
    def tag(self):
        return ", ".join([t.name for t in self.tags.all()])
