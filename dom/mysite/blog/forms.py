# -*- encoding: utf-8 -*-
from django import forms
from models import Entry, User, Tags

class EntryForm(forms.Form):
    OPTIONS = []
    for option in Tags.objects.all():
        OPTIONS.append([option.pk, option])
    title = forms.CharField(label='Temat')
    text = forms.CharField(label=u'Wiadomość',max_length=200, min_length=10, widget=forms.Textarea)
    tags = forms.MultipleChoiceField(label='Tagi',choices=OPTIONS,  widget=forms.CheckboxSelectMultiple)